#!/usr/bin/env python3

inventario = {}


def insertar(codigo: str, nombre: str, precio: float, cantidad: int):
    # inventario[codigo] = [nombre, precio, cantidad]
    valores = {"nombre": nombre, "precio": precio, "cantidad": cantidad}
    inventario[codigo] = valores


def listar():
    for codigo in inventario:
        valores = inventario[codigo]
        print(f"{codigo}: {valores['nombre']}, {valores['precio']}, {valores['cantidad']}")


def consultar(codigo: str):
    if codigo in inventario:
        valores = inventario[codigo]
        print(f"{codigo}: {valores['nombre']}, {valores['precio']}, {valores['cantidad']}")
    else:
        print(f"El código {codigo} no existe")


def agotados():
    for codigo in inventario:
        valores = inventario[codigo]
        if valores['cantidad'] == 0:
            print(f"{codigo}: {valores['nombre']}, {valores['precio']}, {valores['cantidad']}")


def pide_articulo() -> (str, str, float, int):
    codigo = input("Código de altículo: ")
    nombre = input("Nombre: ")
    precio = float(input("Precio: "))
    cantidad = int(input("Cantidad: "))

    return codigo, nombre, precio, cantidad


def menu() -> int:
    print("1. Insertar un artículo")
    print("2. Listar artículos")
    print("3. Consultar artículo")
    print("4. Artículos agotados")
    print("0. Salir")
    opcion = input("Opción: ")

    return opcion


def main():
    seguir = True
    while seguir:
        opcion = menu()
        if opcion == '0':
            seguir = False
        elif opcion == '1':
            ...
        elif opcion == '2':
            listar()
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()
